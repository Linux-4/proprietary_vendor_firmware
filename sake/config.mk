FIRMWARE_IMAGES := \
    abl \
    aop \
    bluetooth \
    cpucp \
    devcfg \
    dsp \
    featenabler \
    hyp \
    keymaster \
    modem \
    multiimgoem \
    qupfw \
    qweslicstore \
    shrm \
    tz \
    uefisecapp \
    xbl \
    xbl_config \
    xrom

AB_OTA_PARTITIONS += $(FIRMWARE_IMAGES)
