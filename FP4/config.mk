AB_OTA_PARTITIONS += \
    bluetooth \
    core_nhlos \
    modem \
    abl \
    aop \
    devcfg \
    dsp \
    featenabler \
    hyp \
    imagefv \
    keymaster \
    multiimgoem \
    qupfw \
    tz \
    uefisecapp \
    xbl \
    xbl_config
