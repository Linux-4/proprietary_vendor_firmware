FIRMWARE_IMAGES := \
    abl \
    bluetooth \
    cmnlib \
    cmnlib64 \
    dsp \
    hyp \
    keymaster \
    modem \
    nvdef \
    rpm \
    tz \
    xbl

AB_OTA_PARTITIONS += $(FIRMWARE_IMAGES)
