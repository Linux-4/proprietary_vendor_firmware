FIRMWARE_IMAGES := \
    ImageFv \
    abl \
    aop \
    bluetooth \
    cmnlib \
    cmnlib64 \
    devcfg \
    dsp \
    hyp \
    keymaster \
    modem \
    qupfw \
    storsec \
    tz \
    xbl \
    xbl_config

AB_OTA_PARTITIONS += $(FIRMWARE_IMAGES)
